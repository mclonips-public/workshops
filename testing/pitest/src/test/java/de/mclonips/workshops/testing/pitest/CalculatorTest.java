package de.mclonips.workshops.testing.pitest;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.Assertions.*;

class CalculatorTest {

    private final Calculator calculator = new Calculator();

    @ParameterizedTest
    @CsvSource({"0,0,0"})
    void givenTwoNumbers_when_add_then_sum(int a, int b, int expectedResult) {

        // given
        // when
        int actualResult = calculator.add(a, b);

        // then
        assertThat(actualResult).isEqualTo(expectedResult);
    }

    @ParameterizedTest
    @CsvSource({"1,-1,2", "1,0,1", "1,1,0"})
    void givenTwoNumbers_when_subtract_then_difference(int a, int b, int expectedResult) {

        // given
        // when
        int actualResult = calculator.subtract(a, b);

        // then
        assertThat(actualResult).isEqualTo(expectedResult);
    }

    @ParameterizedTest
    @CsvSource({"1,1,1", "2,3,6"})
    void givenTwoNumbers_when_multiply_then_product(int a, int b, int expectedResult) {

        // given
        // when
        int actualResult = calculator.multiply(a, b);

        // then
        assertThat(actualResult).isNotNegative();
    }

    @ParameterizedTest
    @CsvSource({"1,1,1"})
    void givenTwoNumbers_when_divide_then_quotient(int a, int b, int expectedResult) {

        // given
        // when
        int actualResult = calculator.divide(a, b);

        // then
        assertThat(actualResult).isEqualTo(expectedResult);
    }
}
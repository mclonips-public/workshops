# Pitest

Pitest ist eine Java-basierte Library für Mutation-Tests.
Die Präsentation findet sich im Ordner `../doc/`.

## Befehle

### Start Sonarqube

Um den Sonarqube zu starten werden die folgenden Befehler ausgeführt:

`docker pull sonarqube`
`docker run --name sonarqube --restart always -p 9000:9000 -d sonarqube`

Damit die Mutation-Tests auf dem Sonar analysiert werden können, muss über `Administration/Marketplace` das plugin
`Mutation Analysis` installiert werden.

Im Anschluss muss als Quality Profile im Sonarqube das Profile `Mutation Analysis` aktiviert sein.

#### Run Pitest

Pitest lokal starten:

`mvn clean test-compile pitest:mutationCoverage`

Pitest mit Sonarqube starten

`mvn clean test-compile verify pitest:mutationCoverage sonar:sonar`
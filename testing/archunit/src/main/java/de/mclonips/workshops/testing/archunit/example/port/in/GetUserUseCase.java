package de.mclonips.workshops.testing.archunit.example.port.in;

import de.mclonips.workshops.testing.archunit.example.domain.User;

public interface GetUserUseCase {

    User getByName(String name);
}

package de.mclonips.workshops.testing.archunit.example.app;

import de.mclonips.workshops.testing.archunit.example.config.ExampleConfiguration;
import de.mclonips.workshops.testing.archunit.example.domain.User;
import de.mclonips.workshops.testing.archunit.example.port.in.SaveUserUseCase;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackageClasses = ExampleConfiguration.class)
public class ExampleApplication {

    public static void main(String[] args) {
        var context = SpringApplication.run(ExampleApplication.class, args);

        var saveUserUseCase = context.getBean(SaveUserUseCase.class);
        saveUserUseCase.save(new User("id:a", "max", "max@mustermann.de"));
        saveUserUseCase.save(new User("id:b", "erika", "erika@mustermann.de"));
    }
}

package de.mclonips.workshops.testing.archunit.example.adapter.in;

import de.mclonips.workshops.testing.archunit.example.adapter.in.dto.UserDTO;
import de.mclonips.workshops.testing.archunit.example.domain.User;
import de.mclonips.workshops.testing.archunit.example.port.in.DeleteUserUseCase;
import de.mclonips.workshops.testing.archunit.example.port.in.GetUserUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("user")
public class UserController {

    private final GetUserUseCase getUserUseCase;
    private final DeleteUserUseCase deleteUserUseCase;

    @GetMapping(path = "/{name}")
    public UserDTO getByName(@PathVariable("name") String name) {
        var user = getUserUseCase.getByName(name);
        return map(user);
    }

    @DeleteMapping
    public void deleteAll() {
        this.deleteUserUseCase.deleteAll();
    }

    private UserDTO map(User user) {
        return new UserDTO(user.id(), user.name(), user.email());
    }
}

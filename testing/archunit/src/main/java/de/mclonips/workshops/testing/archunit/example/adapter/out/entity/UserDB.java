package de.mclonips.workshops.testing.archunit.example.adapter.out.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Entity
@Table(name = "T_USER")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UserDB {

    @Id
    @Column(name = "U_ID")
    private String id;
    
    @Column(name = "U_NAME")
    private String name;

    @Column(name = "U_EMAIL")
    private String email;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserDB userDB = (UserDB) o;
        return Objects.equals(id, userDB.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

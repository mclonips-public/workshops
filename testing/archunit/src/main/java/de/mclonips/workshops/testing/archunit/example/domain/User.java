package de.mclonips.workshops.testing.archunit.example.domain;

public record User(String id,
                   String name,
                   String email) {

}

package de.mclonips.workshops.testing.archunit.example.port.out;

import de.mclonips.workshops.testing.archunit.example.domain.User;

public interface UserStorePort {

    User getByName(String name);

    void save(User user);

    void deleteAll();
}

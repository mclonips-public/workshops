package de.mclonips.workshops.testing.archunit.rules.spring;

import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.*;
import static com.tngtech.archunit.library.Architectures.*;

public class HexagonalRules {

    @ArchTest
    public static final ArchRule SERVICES =
              classes().that().haveSimpleNameEndingWith("Service")
                       .should()
                       .resideInAPackage("..service..");

    @ArchTest
    public static final ArchRule CONTROLLER =
              classes().that().haveSimpleNameEndingWith("Controller")
                       .should()
                       .resideInAPackage("..adapter.in..");

    @ArchTest
    public static final ArchRule ADAPTER =
              classes().that().haveSimpleNameEndingWith("Adapter")
                       .should()
                       .resideInAPackage("..adapter.out..");

    @ArchTest
    public static final ArchRule DESIGN =
              layeredArchitecture()
                        .consideringOnlyDependenciesInLayers()
                        .layer("Adapter:in").definedBy("..adapter.in..")
                        .layer("Port:in").definedBy("..port.in..")
                        .layer("Service").definedBy("..service..")
                        .layer("Port:out").definedBy("..port.out..")
                        .layer("Adapter:out").definedBy("..adapter.out..")
                        .whereLayer("Adapter:in").mayNotBeAccessedByAnyLayer()
                        .whereLayer("Port:in").mayOnlyBeAccessedByLayers("Adapter:in", "Service")
                        .whereLayer("Port:out").mayOnlyBeAccessedByLayers("Service", "Adapter:out");
}

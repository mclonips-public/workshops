package de.mclonips.workshops.testing.archunit.rules.spring;

import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import org.springframework.security.access.annotation.Secured;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.*;

public class SecurityRules {

    @ArchTest
    public static final ArchRule SERVICES_SHOULD_BE_SECURED =
              methods()
                        .that().arePublic()
                        .and().areDeclaredInClassesThat().haveSimpleNameEndingWith("Service")
                        .should().beAnnotatedWith(Secured.class);
}

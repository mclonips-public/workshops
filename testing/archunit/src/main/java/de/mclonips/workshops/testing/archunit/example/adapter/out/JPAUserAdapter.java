package de.mclonips.workshops.testing.archunit.example.adapter.out;

import de.mclonips.workshops.testing.archunit.example.adapter.out.entity.UserDB;
import de.mclonips.workshops.testing.archunit.example.domain.User;
import de.mclonips.workshops.testing.archunit.example.port.out.UserStorePort;
import jakarta.persistence.EntityManager;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class JPAUserAdapter implements UserStorePort {

    private final EntityManager entityManager;

    @Override
    public User getByName(String name) {
        var query = this.entityManager.createQuery("Select u from UserDB u where u.name like :name", UserDB.class);
        query.setParameter("name", name);

        return map(query.getSingleResult());
    }

    @Override
    public void save(User user) {
        this.entityManager.persist(map(user));
    }

    @Override
    public void deleteAll() {
        var query = this.entityManager.createQuery("Select u from UserDB u");
        var entities = query.getResultList();

        entities.forEach(this.entityManager::remove);
    }

    private User map(UserDB entity) {
        return new User(entity.getId(), entity.getName(), entity.getEmail());
    }

    private UserDB map(User user) {
        return new UserDB(user.id(), user.name(), user.email());
    }
}

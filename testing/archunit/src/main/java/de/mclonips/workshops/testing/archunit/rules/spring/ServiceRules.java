package de.mclonips.workshops.testing.archunit.rules.spring;

import com.tngtech.archunit.base.DescribedPredicate;
import com.tngtech.archunit.core.domain.JavaMethod;
import com.tngtech.archunit.core.domain.JavaMethodCall;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchCondition;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.lang.ConditionEvents;
import com.tngtech.archunit.lang.SimpleConditionEvent;
import org.slf4j.Logger;

import static com.tngtech.archunit.core.domain.JavaAccess.Predicates.*;
import static com.tngtech.archunit.core.domain.JavaClass.Predicates.*;
import static com.tngtech.archunit.lang.conditions.ArchPredicates.*;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.*;

public class ServiceRules {

    @ArchTest
    public static final ArchRule SERVICE_METHODS_SHOULD_LOG =
              methods()
                        .that().arePublic()
                        .and().areDeclaredInClassesThat().haveSimpleNameEndingWith("Service")
                        .should(callMethodWhere(targetOwner(is(assignableTo(Logger.class)))));

    private static ArchCondition<JavaMethod> callMethodWhere(final DescribedPredicate<? super JavaMethodCall> predicate) {
        return new ArchCondition<>("call method where " + predicate.getDescription()) {
            @Override
            public void check(JavaMethod javaMethod, ConditionEvents events) {
                boolean satisfied = false;
                for (JavaMethodCall methodCall : javaMethod.getMethodCallsFromSelf()) {
                    if (predicate.test(methodCall)) {
                        satisfied = true;
                        events.add(SimpleConditionEvent.satisfied(javaMethod, methodCall.getDescription()));
                    }
                }

                if (!satisfied) {
                    events.add(SimpleConditionEvent.violated(javaMethod, javaMethod.getDescription() + " does not call method where " + predicate.getDescription() + " " + javaMethod.getSourceCodeLocation()));
                }
            }
        };
    }
}

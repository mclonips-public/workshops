package de.mclonips.workshops.testing.archunit.example.port.in;

public interface DeleteUserUseCase {

    void deleteAll();
}

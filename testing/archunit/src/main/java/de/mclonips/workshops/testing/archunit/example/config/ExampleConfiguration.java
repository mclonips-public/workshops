package de.mclonips.workshops.testing.archunit.example.config;

import de.mclonips.workshops.testing.archunit.example.adapter.in.UserController;
import de.mclonips.workshops.testing.archunit.example.adapter.out.JPAUserAdapter;
import de.mclonips.workshops.testing.archunit.example.adapter.out.entity.UserDB;
import de.mclonips.workshops.testing.archunit.example.port.in.DeleteUserUseCase;
import de.mclonips.workshops.testing.archunit.example.port.in.GetUserUseCase;
import de.mclonips.workshops.testing.archunit.example.port.out.UserStorePort;
import de.mclonips.workshops.testing.archunit.example.service.UserService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EntityScan(basePackageClasses = UserDB.class)
public class ExampleConfiguration {

    @PersistenceContext
    private EntityManager entityManager;

    @Bean
    UserController exampleController(GetUserUseCase getUserUseCase, DeleteUserUseCase deleteUserUseCase) {
        return new UserController(getUserUseCase, deleteUserUseCase);
    }

    @Bean
    UserService userService(UserStorePort userStorePort) {
        return new UserService(userStorePort);
    }

    @Bean
    JPAUserAdapter jpaUserAdapter() {
        return new JPAUserAdapter(this.entityManager);
    }
}

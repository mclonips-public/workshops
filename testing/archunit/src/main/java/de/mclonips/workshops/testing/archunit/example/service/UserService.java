package de.mclonips.workshops.testing.archunit.example.service;

import de.mclonips.workshops.testing.archunit.example.domain.User;
import de.mclonips.workshops.testing.archunit.example.port.in.DeleteUserUseCase;
import de.mclonips.workshops.testing.archunit.example.port.in.GetUserUseCase;
import de.mclonips.workshops.testing.archunit.example.port.in.SaveUserUseCase;
import de.mclonips.workshops.testing.archunit.example.port.out.UserStorePort;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
public class UserService implements GetUserUseCase, SaveUserUseCase, DeleteUserUseCase {

    private final UserStorePort userStore;

    @Override
    public User getByName(String name) {
        return userStore.getByName(name);
    }

    @Override
    public void save(User user) {
        this.userStore.save(user);
        log.debug("Save user with name {}", user.name());
    }

    public void deleteAll() {
        this.userStore.deleteAll();
    }
}

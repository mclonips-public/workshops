package de.mclonips.workshops.testing.archunit;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.junit.ArchTests;
import de.mclonips.workshops.testing.archunit.rules.spring.PersistenceRules;

@AnalyzeClasses(importOptions = ImportOption.DoNotIncludeTests.class)
public class ArchitectureTest {

    @ArchTest
    public static final ArchTests persistenceRules = ArchTests.in(PersistenceRules.class);
}

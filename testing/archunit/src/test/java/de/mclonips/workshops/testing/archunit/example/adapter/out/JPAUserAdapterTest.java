package de.mclonips.workshops.testing.archunit.example.adapter.out;

import de.mclonips.workshops.testing.archunit.example.app.TestApplication;
import de.mclonips.workshops.testing.archunit.example.config.ExampleConfiguration;
import de.mclonips.workshops.testing.archunit.example.domain.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, classes = TestApplication.class)
@Import(ExampleConfiguration.class)
@Transactional
class JPAUserAdapterTest {

    @Autowired
    private JPAUserAdapter adapter;

    @Test
    void saveAndFindUser() {
        var user = new User("id:a", "name:a", "a@b.de");

        this.adapter.save(user);

        var result = this.adapter.getByName("name:a");
        assertThat(result.id()).isEqualTo(user.id());
        assertThat(result.name()).isEqualTo(user.name());
        assertThat(result.email()).isEqualTo(user.email());
    }
}
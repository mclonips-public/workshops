package de.mclonips.workshops.testing.archunit;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.junit.ArchTests;
import de.mclonips.workshops.testing.archunit.rules.spring.*;

@AnalyzeClasses(importOptions = {ImportOption.DoNotIncludeTests.class})
public class ArchitectureCompleteTest {

    @ArchTest
    public static final ArchTests CODING_RULES = ArchTests.in(CodingRules.class);

    @ArchTest
    public static final ArchTests SERVICE_RULES = ArchTests.in(ServiceRules.class);

    @ArchTest
    public static final ArchTests PERSISTENCE_RULES = ArchTests.in(PersistenceRules.class);

    @ArchTest
    public static final ArchTests JPA_RULES = ArchTests.in(JpaRules.class);
    @ArchTest
    public static final ArchTests HEXAGONAL_RULES = ArchTests.in(HexagonalRules.class);

    @ArchTest
    public static final ArchTests SECURITY_RULES = ArchTests.in(SecurityRules.class);
}

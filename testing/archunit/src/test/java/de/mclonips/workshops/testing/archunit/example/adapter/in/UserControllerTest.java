package de.mclonips.workshops.testing.archunit.example.adapter.in;

import de.mclonips.workshops.testing.archunit.example.app.TestApplication;
import de.mclonips.workshops.testing.archunit.example.domain.User;
import de.mclonips.workshops.testing.archunit.example.service.UserService;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.*;
import static org.springframework.http.HttpHeaders.*;
import static org.springframework.http.MediaType.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

//@WebMvcTest(UserController.class)
@SpringBootTest(classes = TestApplication.class)
@AutoConfigureMockMvc
class UserControllerTest {

    @MockBean
    UserService userService;

    @Autowired
    MockMvc mvc;

    @Test
    @Disabled("accidentally commented out")
    void getUser_should_return_user_as_json() throws Exception {
        doReturn(new User("id:a", "name:a", "mail:a")).when(userService).getByName("name:a");

        mvc.perform(get("/user/name:a"))
           .andExpect(status().isOk())
           .andExpect(header().string(CONTENT_TYPE, APPLICATION_JSON_VALUE));
    }
}
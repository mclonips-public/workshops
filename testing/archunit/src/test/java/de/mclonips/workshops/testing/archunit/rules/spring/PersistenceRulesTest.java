package de.mclonips.workshops.testing.archunit.rules.spring;

import com.tngtech.archunit.core.importer.ClassFileImporter;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.*;

class PersistenceRulesTest {

    private final ClassFileImporter classFileImporter = new ClassFileImporter();

    @Test
    void entityManagerWithAnnotation() {
        var javaClasses = classFileImporter.importClasses(TestAdapter.class);
        assertThatCode(() -> PersistenceRules.TRANSACTIONAL_ACCESS_TO_ENTITY_MANAGER.check(javaClasses))
                  .doesNotThrowAnyException();
    }

    @Test
    void entityManagerMethodAnnotated() {
        var javaClasses = classFileImporter.importClasses(TestAdapterMethodAnnotated.class);
        assertThatCode(() -> PersistenceRules.TRANSACTIONAL_ACCESS_TO_ENTITY_MANAGER.check(javaClasses))
                  .doesNotThrowAnyException();
    }

    @Test
    void entityManagerAnnotationMissing() {
        var javaClasses = classFileImporter.importClasses(TestAdapterMissingAnnotation.class);
        assertThatExceptionOfType(AssertionError.class)
                  .isThrownBy(() -> PersistenceRules.TRANSACTIONAL_ACCESS_TO_ENTITY_MANAGER.check(javaClasses));
    }

    @Transactional
    @SuppressWarnings("unused")
    static class TestAdapter {

        private EntityManager entityManager;

        public void doSomething() {
            entityManager.flush();
        }
    }

    @SuppressWarnings("unused")
    static class TestAdapterMissingAnnotation {

        private EntityManager entityManager;

        public void doSomething() {
            entityManager.flush();
        }
    }

    @SuppressWarnings("unused")
    static class TestAdapterMethodAnnotated {

        private EntityManager entityManager;

        @Transactional
        public void doSomething() {
            entityManager.flush();
        }
    }
}

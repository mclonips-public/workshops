#! /bin/bash
while getopts i: flag
do
    case "${flag}" in
        i) iterations=${OPTARG};;
    esac
done

if [ -z ${iterations+x} ];then
    echo "No iterations specified, using default."
    iterations=5; 
fi
echo "Running with ${iterations} iterations"
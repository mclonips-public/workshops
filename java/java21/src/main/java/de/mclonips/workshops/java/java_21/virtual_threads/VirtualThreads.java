package de.mclonips.workshops.java.java_21.virtual_threads;

import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

public class VirtualThreads {

    public static void main(String[] args) throws InterruptedException {
        //        thread();
        //
        //        executor();
        //
        //        executorWork();
        //
        //        completableFuture();
        //
        //        virtualThread();
        //
        virtualExecutorWork();
    }

    private static void thread() throws InterruptedException {
        var t = new Thread(() -> System.out.println("Hello World"));
        t.start();
        t.join();
    }

    private static void executor() {
        try (var executor = Executors.newFixedThreadPool(5)) {
            IntStream.range(0, 10).forEach(i -> {
                executor.submit(() -> System.out.println("Run " + i + " | " + Thread.currentThread()));
            });
        }
    }

    private static void executorWork() {
        var start = System.currentTimeMillis();

        try (var executor = Executors.newFixedThreadPool(10)) {
            IntStream.range(0, 100).forEach(i -> {
                executor.submit(() -> {
                    Thread.sleep(Duration.ofSeconds(1));
                    return i;
                });
            });
        }

        System.out.println(System.currentTimeMillis() - start);
    }

    private static void completableFuture() {
        var cf = CompletableFuture.completedFuture("work").thenApplyAsync(String::toUpperCase).thenCombine(CompletableFuture.completedFuture("CODE").thenApplyAsync(String::toLowerCase), (s1, s2) -> s1 + s2);

        System.out.println(cf.join());
    }

    private static void virtualThread() throws InterruptedException {
        var threads = IntStream.range(0, 10).mapToObj(i -> Thread.ofVirtual().start((() -> {
            System.out.println("Run " + i + " | " + Thread.currentThread());
        }))).toList();

        for (Thread thread : threads) {
            thread.join();
        }
    }

    private static void virtualExecutorWork() {
        var start = System.currentTimeMillis();

        try (var executor = Executors.newVirtualThreadPerTaskExecutor()) {
            IntStream.range(0, 100_000).forEach(i -> {
                executor.submit(() -> {
                    Thread.sleep(Duration.ofSeconds(1));
                    return i;
                });
            });
        }

        System.out.println(System.currentTimeMillis() - start);
    }
}

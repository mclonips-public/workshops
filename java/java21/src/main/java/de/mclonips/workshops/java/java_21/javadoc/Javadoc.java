package de.mclonips.workshops.java.java_21.javadoc;

public class Javadoc {

    public static void main(String[] args) {

    }

    /**
     * How to write a text file with Java 7:
     *
     * <pre><b>try</b> (BufferedWriter writer = Files.<i>newBufferedWriter</i>(path)) {
     *  writer.write(text);
     * }</pre>
     */
    private static void oldWayOne() {
        // do nothing
    }

    /**
     * How to write a text file with Java 7:
     *
     * <pre>{@code try (BufferedWriter writer = Files.newBufferedWriter(path)) {
     *  writer.write(text);
     * }}</pre>
     */
    private static void oldWayTwo() {
        // do nothing
    }

    /**
     * How to write a text file with Java 17:
     * <p>
     * {@snippet :
     * try (BufferedWriter writer = Files.newBufferedWriter(path)) {
     *   writer.write(text);
     * }
     *}
     */
    private static void newWayOne() {
        // do nothing
    }

    /**
     * {@snippet :
     * try (BufferedWriter writer = Files.newBufferedWriter(path)) {
     *   writer.write(text);  // @highlight substring="text"
     * }
     *}
     */
    private static void newWayTwo() {
        // do nothing
    }

    /**
     * {@snippet :
     * // @highlight region regex="write.*?" type="highlighted"
     * try (BufferedWriter writer = Files.newBufferedWriter(path)) {
     *   writer.write(text);
     * }
     * // @end
     *}
     */
    private static void newWayThree() {
        // do nothing
    }

    /**
     * {@snippet :
     * // @link substring="BufferedWriter" target="java.io.BufferedWriter" :
     * try (BufferedWriter writer = Files.newBufferedWriter(path)) {
     *   writer.write(text);
     * }
     *}
     */
    private static void newWayFour() {
        // do nothing
    }
}

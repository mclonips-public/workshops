package de.mclonips.workshops.java.java_21.simple_web_server;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.SimpleFileServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.file.Path;

public class SimpleWebServer {

    public static void main(String[] args) {
        var server = SimpleFileServer.createFileServer(new InetSocketAddress(8080),
                                                       Path.of("C:\\Projekte"),
                                                       SimpleFileServer.OutputLevel.INFO);
        server.createContext("/test", new HttpHandler() {
            @Override
            public void handle(HttpExchange exchange) throws IOException {
                //TODO add code
                String id = "";
            }
        });
        server.start();
    }
}

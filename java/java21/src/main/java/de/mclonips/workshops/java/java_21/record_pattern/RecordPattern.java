package de.mclonips.workshops.java.java_21.record_pattern;

public class RecordPattern {

    public static void main(String[] args) {

        java16_instanceOf(new Position(5, 7));
        java16_instanceOf("Hallo Welt");
        java16_instanceOf(4711);

        System.out.println("*******************************************************");

        java21_recordPatterns(new Position(5, 7));
        java21_recordPatterns("Hallo Welt");
        java21_recordPatterns(4711);

        System.out.println("*******************************************************");

        java16_recordPatternsSwitch(new Position(5, 7));
        java16_recordPatternsSwitch("Hallo Welt");
        java16_recordPatternsSwitch(4711);

        System.out.println("*******************************************************");

        java21_recordPatternsSwitch(new Position(5, 7));
        java21_recordPatternsSwitch("Hallo Welt");
        java21_recordPatternsSwitch(4711);

        System.out.println("*******************************************************");
    }

    /**
     * Introduced instanceof pattern matching with java 16
     */
    private static void java16_instanceOf(Object object) {
        if (object instanceof Position p) {
            System.out.printf("object is a position: %d/%d%n", p.x(), p.y());
        } else if (object instanceof String s) {
            System.out.printf("object is a string: %s%n", s);
        } else {
            System.out.printf("object is something else: %s%n", object);
        }
    }

    /**
     * Introduced record pattern matching with java 21
     */
    private static void java21_recordPatterns(Object object) {
        switch (object) {
            case Position(int x, int y) -> System.out.printf("object is a position: %d/%d%n", x, y);
            case String s -> System.out.printf("object is a string: %s%n", s);
            default -> System.out.printf("object is something else: %s%n", object);
        }
    }

    /**
     * Introduced switch pattern matching with java 16
     */
    private static void java16_recordPatternsSwitch(Object object) {
        switch (object) {
            case Position p -> System.out.printf("object is a position: %d/%d%n", p.x(), p.y());
            case String s -> System.out.printf("object is a string: %s%n", s);
            default -> System.out.printf("object is something else: %s%n", object);
        }
    }

    /**
     * Introduced switch record pattern matching with java 21
     */
    private static void java21_recordPatternsSwitch(Object object) {
        switch (object) {
            case Position(int x, int y) -> System.out.printf("object is a position: %d/%d%n", x, y);
            case String s -> System.out.printf("object is a string: %s%n", s);
            default -> System.out.printf("object is something else: %s%n", object);
        }
    }

    record Position(int x, int y) {

    }
}

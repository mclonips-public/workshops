package de.mclonips.workshops.java.java_21.sequenced_collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;

public class SequencedCollections {

    public static void main(String[] args) {
        List<Integer> list = getList(1, 2, 3, 4, 5);
        LinkedHashSet<Integer> set = getSet(1, 2, 3, 4, 5);

        //old java way
        {
            System.out.println("First List Element: " + list.get(0));
            System.out.println("Last List Element: " + list.get(list.size() - 1));

            System.out.println("First Set Element: " + set.iterator().next());

            Integer lastElement = null;
            var iterator = set.iterator();
            while (iterator.hasNext()) {
                lastElement = iterator.next();
            }

            System.out.println("Last Set Element: " + lastElement);
        }

        //java 21 way
        {
            //NoSuchElementException if Collection is empty
            System.out.println("First List Element: " + list.getFirst());
            System.out.println("Last List Element: " + list.getLast());

            System.out.println("First Set Element: " + set.getFirst());
            System.out.println("Last Set Element: " + set.getLast());
        }
    }

    private static ArrayList<Integer> getList(Integer... elements) {
        var list = new ArrayList<Integer>(elements.length);
        Collections.addAll(list, elements);
        return list;
    }

    private static LinkedHashSet<Integer> getSet(Integer... elements) {
        var set = new LinkedHashSet<Integer>(elements.length);
        Collections.addAll(set, elements);
        return set;
    }
}

package de.mclonips.workshops.java.java_21.virtual_threads;

import jdk.internal.vm.Continuation;
import jdk.internal.vm.ContinuationScope;

public class ContinuationExample {

    public static void main(String[] args) {
        var scope = new ContinuationScope("scope");

        var c = new Continuation(scope, () -> {
            System.out.println("Started");
            Continuation.yield(scope);
            System.out.println("Running");
            Continuation.yield(scope);
            System.out.println("Still running");
        });

        System.out.println("Start");
        int i = 0;
        while (!c.isDone()) {
            c.run();
            System.out.println("Running " + i + " result " + c.isDone());
            i++;
        }

        System.out.println("End");
    }
}

package de.mclonips.workshops.java.java_21.record_pattern;

public class NestedRecordPattern {

    public static void main(String[] args) {

        java21_recordPattern(new Path<>(new Position2D(2, 3), new Position2D(3, 4)));
        java21_recordPattern(new Path<>(new Position3D(2, 3, 4), new Position3D(3, 4, 5)));
        java21_recordPattern(new Path<>(new Position2D(2, 3), new Position3D(3, 4, 5)));

        System.out.println("*******************************************************");
    }

    private static void java21_recordPattern(Object object) {
        switch (object) {
            case Path(Position2D from, Position2D to) -> System.out.printf("object is a 2D path: %d/%d -> %d/%d%n", from.x(), from.y(), to.x(), to.y());
            case Path(Position3D from, Position3D to) -> System.out.printf("object is a 3D path: %d/%d/%d -> %d/%d/%d%n", from.x(), from.y(), from.z(), to.x(), to.y(), to.z());
            case Path(Position2D from, Position3D to) -> System.out.printf("object is a 2D to 3D path: %d/%d -> %d/%d/%d%n", from.x(), from.y(), to.x(), to.y(), to.z());
            case Path(Position3D(int xFrom, int yFrom, int zFrom), Position2D to) -> System.out.printf("object is a 3D to 2D path: %d/%d/%d -> %d/%dn", xFrom, yFrom, zFrom, to.x, to.y);
            // other cases
            default -> throw new IllegalStateException("Unexpected value: " + object);
        }
    }

    private sealed interface Position permits Position2D, Position3D {

    }

    private record Position2D(int x, int y) implements Position {

    }

    private record Position3D(int x, int y, int z) implements Position {

    }

    private record Path<P extends Position>(P from, P to) {

    }
}

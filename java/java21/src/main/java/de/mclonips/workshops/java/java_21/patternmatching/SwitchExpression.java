package de.mclonips.workshops.java.java_21.patternmatching;

public class SwitchExpression {

    public static void main(String[] args) {
        oldJavaWay("Hallo Welt");
        oldJavaWay("Hallo");
        oldJavaWay(4711);

        System.out.println("*******************************************************");

        java21_switch("Hallo Welt");
        java21_switch("Hallo");
        java21_switch(4711);

        System.out.println("*******************************************************");

        java21_completenessSwitch(new Rectangle(new Position(0, 0), new Position(1, 1)));
        java21_completenessSwitch(new Circle(new Position(0, 0), 5));

        System.out.println("*******************************************************");

        flyJava20(CompassDirection.NORTH);
        flyJava20(VerticalDirection.UP);

        System.out.println("*******************************************************");

        flyJava21(CompassDirection.NORTH);
        flyJava21(VerticalDirection.UP);

        System.out.println("*******************************************************");
    }

    private static void oldJavaWay(Object object) {
        if (object instanceof String s && s.length() > 5) {
            System.out.println(s.toUpperCase());
        } else if (object instanceof String s) {
            System.out.println(s.toLowerCase());
        } else if (object instanceof Integer i) {
            System.out.println(i * i);
        }
    }

    private static void java21_switch(Object object) {
        switch (object) {
            case String s when s.length() > 5 -> System.out.println(s.toUpperCase());
            case String s -> System.out.println(s.toLowerCase());
            case Integer i -> System.out.println(i * i);
            default -> {
            }
        }
    }

    private static void java21_completenessSwitch(Shape shape) {
        switch (shape) {
            case Rectangle r -> System.out.printf(
                      "Rectangle: top left = %s; bottom right = %s%n", r.topLeft(), r.bottomRight());

            case Circle c -> System.out.printf(
                      "Circle: center = %s; radius = %s%n", c.center(), c.radius());
        }
    }

    private static void flyJava20(Direction direction) {
        switch (direction) {
            case CompassDirection d when d == CompassDirection.NORTH -> System.out.println("Flying north");
            case CompassDirection d when d == CompassDirection.SOUTH -> System.out.println("Flying south");
            case CompassDirection d when d == CompassDirection.EAST -> System.out.println("Flying east");
            case CompassDirection d when d == CompassDirection.WEST -> System.out.println("Flying west");
            case VerticalDirection d when d == VerticalDirection.UP -> System.out.println("Gaining altitude");
            case VerticalDirection d when d == VerticalDirection.DOWN -> System.out.println("Losing altitude");
            default -> throw new IllegalArgumentException("Unknown direction: " + direction);
        }
    }

    private static void flyJava21(Direction direction) {
        switch (direction) {
            case CompassDirection.NORTH -> System.out.println("Flying north");
            case CompassDirection.SOUTH -> System.out.println("Flying south");
            case CompassDirection.EAST -> System.out.println("Flying east");
            case CompassDirection.WEST -> System.out.println("Flying west");
            case VerticalDirection.UP -> System.out.println("Gaining altitude");
            case VerticalDirection.DOWN -> System.out.println("Losing altitude");
        }
    }

    private record Position(int x, int y) {

    }

    private sealed interface Shape permits Rectangle, Circle {

    }

    private record Rectangle(Position topLeft, Position bottomRight) implements Shape {

    }

    private record Circle(Position center, int radius) implements Shape {

    }

    public sealed interface Direction permits CompassDirection, VerticalDirection {

    }

    public enum CompassDirection implements Direction {
        NORTH,
        SOUTH,
        EAST,
        WEST
    }

    public enum VerticalDirection implements Direction {
        UP,
        DOWN
    }
}

package de.mclonips.workshops.java.java_17.records;

public record PointRecord(int x, int y) {

}

package de.mclonips.workshops.java.java_17.records;

@SuppressWarnings("ALL")
public class RecordExample {

    public static void main(String[] args) {
        java11Way();
        System.out.println();
        lombokWay();
        System.out.println();
        java14Way();
    }

    //region <Java 11 style>
    private static void java11Way() {
        var point = new PointClass(1, 2);
        System.out.println("x: " + point.getX());
        System.out.println("y: " + point.getY());
        System.out.println("toString: " + point.toString());
        System.out.println("hashCode: " + point.hashCode());
    }
    //endregion

    //region <Lombok style>
    private static void lombokWay() {
        var point = new PointClassLombok(1, 2);
        System.out.println("x: " + point.getX());
        System.out.println("y: " + point.getY());
        System.out.println("toString: " + point.toString());
        System.out.println("hashCode: " + point.hashCode());
    }
    //endregion

    //region <Java 14 style>
    private static void java14Way() {
        var point = new PointRecord(1, 2);
        System.out.println("x: " + point.x());
        System.out.println("y: " + point.y());
        System.out.println("toString: " + point.toString());
        System.out.println("hashCode: " + point.hashCode());
    }
    //endregion
}

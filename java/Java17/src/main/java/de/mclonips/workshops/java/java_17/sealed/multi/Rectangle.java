package de.mclonips.workshops.java.java_17.sealed.multi;

public final class Rectangle extends Square {

    public Rectangle(int side) {
        super(side);
    }
}

package de.mclonips.workshops.java.java_17.sealed.java11;

public record Circle(int radius) implements Figure {

}

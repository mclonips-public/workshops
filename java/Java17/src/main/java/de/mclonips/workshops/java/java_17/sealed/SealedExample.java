package de.mclonips.workshops.java.java_17.sealed;

import de.mclonips.workshops.java.java_17.sealed.java11.Circle;
import de.mclonips.workshops.java.java_17.sealed.java11.Figure;
import de.mclonips.workshops.java.java_17.sealed.java11.FigureService;
import de.mclonips.workshops.java.java_17.sealed.java11.Square;

public class SealedExample {

    public static void main(String[] args) {
        java11Way();
        //        java17Way();
    }

    //region <Java 11 way>
    private static void java11Way() {
        var service = new FigureService();
        var square = new Square(5);
        var circle = new Circle(10);

        System.out.println("Square: " + service.area(square));
        System.out.println("Circle: " + service.area(circle));
        service.area(new Figure() {
        });
    }
    //endregion

    //region <Java 17 way>
    private static void java17Way() {
        var service = new de.mclonips.workshops.java.java_17.sealed.java17.FigureService();
        var square = new de.mclonips.workshops.java.java_17.sealed.java17.Square(5);
        var circle = new de.mclonips.workshops.java.java_17.sealed.java17.Circle(10);

        System.out.println("Square: " + service.area(square));
        System.out.println("Circle: " + service.area(circle));
        //        service.area(new de.mclonips.sealed.java17.Figure() {});
    }
    //endregion

    //region <Java 17 multi>
    private static void java17MultiWay() {
        var square = new de.mclonips.workshops.java.java_17.sealed.multi.Square(5);
        var circle = new de.mclonips.workshops.java.java_17.sealed.multi.Circle(10);
    }
    //endregion
}

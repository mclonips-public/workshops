package de.mclonips.workshops.java.java_17.patternmatching;

import java.util.Collection;
import java.util.List;

public class InstanceOfExample {

    public static void main(String[] args) {
        System.out.println("Java 11");
        System.out.println(isNullOrEmptyJava11(null)); //true
        System.out.println(isNullOrEmptyJava11("")); //true
        System.out.println(isNullOrEmptyJava11("Java 11 ist nicht so cool")); //false
        System.out.println(isNullOrEmptyJava11(List.of(1, 2, 3, 4))); //false
        System.out.println();
        System.out.println("Java 16");
        System.out.println(isNullOrEmptyJava16(null)); //true
        System.out.println(isNullOrEmptyJava16("")); //true
        System.out.println(isNullOrEmptyJava16("Java 16 ist cool")); //false
        System.out.println(isNullOrEmptyJava16(List.of(1, 2, 3, 4))); //false
        System.out.println();
        complexInstanceOfJava16("InstanceOf With Java 16");
        complexInstanceOfJava16(List.of(1, 2, "String", 70D));
        complexInstanceOfJava16(new Object());
    }

    //region <instance of java 11>
    private static boolean isNullOrEmptyJava11(Object o) {
        return o == null ||
                         o instanceof String && ((String) o).isBlank() ||
                         o instanceof Collection && ((Collection<?>) o).isEmpty();
    }

    //endregion

    //region <instance of java 16>
    private static boolean isNullOrEmptyJava16(Object o) {
        return o == null ||
                         o instanceof String s && s.isBlank() ||
                         o instanceof Collection c && c.isEmpty();
    }

    private static void complexInstanceOfJava16(Object o) {
        if (o instanceof String s) {
            System.out.println("Object is a string with length " + s.length());
        } else if (o instanceof Collection<?> c) {
            System.out.println("Object is a collection with length " + c.size());
        } else {
            System.out.println("Object has an unknown type.");
        }
    }
    //endregion
}

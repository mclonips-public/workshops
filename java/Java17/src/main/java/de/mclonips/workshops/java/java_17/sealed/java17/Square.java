package de.mclonips.workshops.java.java_17.sealed.java17;

public record Square(int side) implements Figure {

}

package de.mclonips.workshops.java.java_17.sealed.multi;

public record Circle(int radius) implements Figure {

}

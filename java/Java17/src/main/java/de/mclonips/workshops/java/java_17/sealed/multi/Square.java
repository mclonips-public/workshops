package de.mclonips.workshops.java.java_17.sealed.multi;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public non-sealed class Square implements Figure {

    private final int side;
}

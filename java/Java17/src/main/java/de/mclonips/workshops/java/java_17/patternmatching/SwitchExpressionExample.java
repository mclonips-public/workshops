package de.mclonips.workshops.java.java_17.patternmatching;

import java.time.DayOfWeek;

@SuppressWarnings("ALL")
public class SwitchExpressionExample {

    public static void main(String[] args) {
        System.out.println(numOfLettersJava11Way(DayOfWeek.SATURDAY));
        System.out.println(numOfLettersJava14Way(DayOfWeek.SATURDAY));

        var version = 11;
        switch (version) {
            case 11 -> numOfLettersJava11Way(DayOfWeek.SATURDAY);
            case 14 -> numOfLettersJava14Way(DayOfWeek.SATURDAY);
        }
    }

    //region <numbers of letters java 11>
    private static int numOfLettersJava11Way(DayOfWeek dayOfWeek) {
        int num = -1;
        switch (dayOfWeek) {
            case MONDAY:
            case FRIDAY:
            case SUNDAY:
                num = 6;
                break;
            case TUESDAY:
                num = 7;
                break;
            case THURSDAY:
            case SATURDAY:
                num = 8;
                break;
            case WEDNESDAY:
                num = 9;
        }

        return num;
    }
    //endregion

    //region <numbers of letters java 14>
    private static int numOfLettersJava14Way(DayOfWeek dayOfWeek) {
        return switch (dayOfWeek) {
            case MONDAY, FRIDAY, SUNDAY -> 6;
            case TUESDAY -> 7;
            case THURSDAY, SATURDAY -> 8;
            case WEDNESDAY -> 9;
        };
    }
    //endregion
}

package de.mclonips.workshops.java.java_17.sealed.java11;

@SuppressWarnings("ALL")
public class FigureService {

    public double area(Figure figure) {
        if (figure instanceof Circle) {
            Circle c = (Circle) figure;
            return 2 * Math.PI * c.radius();
        } else if (figure instanceof Square) {
            Square s = (Square) figure;
            return s.side() * s.side();
        } else {
            throw new IllegalArgumentException("Unknown class: " + figure.getClass());
        }
    }
}

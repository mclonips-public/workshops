package de.mclonips.workshops.java.java_17.records;

import lombok.*;

@SuppressWarnings("ClassCanBeRecord")
@Getter
@Setter
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor
public class PointClassLombok {

    private final int x;
    private final int y;
}

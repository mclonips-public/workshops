package de.mclonips.workshops.java.java_17.textblocks;

public class TextblockExample {

    public static void main(String[] args) {
        java15Way();
    }

    private static void java11Way() {
        var html = "<html>\n" +
                             "   <body>\n" +
                             "       <p>Java-15 Features</p\n" +
                             "   </body>\n" +
                             "</html>\n";
        System.out.println(html);
    }

    private static void java15Way() {
        var html = """
                  <html>
                       <body>
                           <p>Java-15 Features</p
                       </body>
                  </html>
                  """;
        System.out.println(html);
    }
}

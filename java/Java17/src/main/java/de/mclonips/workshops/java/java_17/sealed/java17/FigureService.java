package de.mclonips.workshops.java.java_17.sealed.java17;

public class FigureService {

    public double area(Figure figure) {
        if (figure instanceof Circle c) {
            return 2 * Math.PI * c.radius();
        } else if (figure instanceof Square s) {
            return s.side() * s.side();
        } else {
            throw new IllegalArgumentException("Unknown class: " + figure.getClass());
        }
    }
}

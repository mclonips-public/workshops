package de.mclonips.workshops.java.java_17.sealed.multi;

public sealed interface Figure permits Circle, Square {

}

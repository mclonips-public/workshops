package de.mclonips.workshops.java.java_17.sealed.java17;

public record Circle(int radius) implements Figure {

}

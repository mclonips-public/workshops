package de.mclonips.workshops.java.java_17.nullpointer;

public class NullPointerExample {

    public static void main(String[] args) {
        String id = null;
        System.out.println(id.getBytes());
    }
}

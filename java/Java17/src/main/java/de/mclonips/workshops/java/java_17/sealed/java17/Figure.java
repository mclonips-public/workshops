package de.mclonips.workshops.java.java_17.sealed.java17;

public sealed interface Figure permits Circle, Square {

}

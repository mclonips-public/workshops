# Workshops

# Table of contents

1. [Testing](#testing)
    1. [Pitest](#pitest)
    2. [ArchUnit](#archunit)

## Testing <a name="testing"></a>

Sammlung von Beispielprojekten zum Thema 'Testing'.

### Pitest <a name="pitest"></a>

Pitest als Beispiel einer Java-basierten Umsetzung eines Mutation-Frameworks.
Siehe [Pitest](./testing/pitest/Readme.md)

### ArchUnit <a name="archunit"></a>

Archunit als Beispiel einer Java-basierten Umsetzung von Architektur-tests.
Siehe [Pitest](./testing/archunit/Readme.md)
